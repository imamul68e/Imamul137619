<?php
$array = array("a"=> 1, "b"=> 2, "c"=> 3, "d" => 4);
echo "<pre>";
print_r($array);

print_r( array_rand($array));	// by default, shows only one key.
echo "<br>";
print_r( array_rand($array, 2));
print_r( array_rand($array, 3));

$input = array("Neo", "Morpheus", "Trinity", "Cypher", "Tank");
$rand_keys = array_rand($input, 2);
echo $input[$rand_keys[0]] . "\n";
echo $input[$rand_keys[1]] . "\n";